#!/usr/bin/python3

import yaml


def main():
    slots = []
    row = 1
    for hour in range(8, 29):
        for minute in [0, 15, 30, 45]:
            row += 1
            for duration in list(range(30, 500, 15)):
                d = dict(
                    key="%02d%02d-%dmin" % (hour, minute, duration),
                    start="%02d:%02d" % (hour % 24, minute),
                    duration=duration,
                    row=dict(start=row),
                )
                d["row"]["end"] = row + int(duration // 15)
                slots.append(d)
    with open("data/slots.yml", "w") as buf:
        yaml.dump(dict(slots=slots), buf)


if __name__ == "__main__":
    main()
