#!/bin/sh

set -eu

git reset --hard HEAD
git ls-files | grep -E '^content/talks/.*\.md$' | xargs -r git rm -f 

curl -s 'https://files.esiliati.org/remote.php/dav/public-calendars/2w2meG65B4p9yfLD?export' > room1.ics
curl -s 'https://files.esiliati.org/remote.php/dav/public-calendars/YpCMj9qgnWjHtAyd?export' > room2.ics
curl -s 'https://files.esiliati.org/remote.php/dav/public-calendars/grWX9kAXpyxiBTDG?export' > room3.ics
curl -s 'https://files.esiliati.org/remote.php/dav/public-calendars/RYWGzPpr2eYdXPX9?export' > extra.ics

mkdir -p content/talks/
# questo viene preso da https://git.lattuga.net/boyska/ics2mdwn
ics2mdwn.py room1.ics room2.ics room3.ics extra.ics | stest -f | xargs -r git add

git commit --author='calendarioBot <calendario@example.com>' -m 'update calendar from remote ICS file'
