
## Talk

### Come funziona

#### Talk

I talk possono essere aggiunti all'interno della cartella `content/talks`. Sono file dall'estensione markdown, `.md`.  
La parte iniziale del file è strutturata con i campi, i metadati, che descrivono il talk. Il formato di questa struttura è TOML. I campi in uso per i talk sono i seguenti:

```toml
+++
title = "Presentazione libro Millenium Bug su Indymedia"
key = "presentazione-libro-millenium-bug-su-indymedia"
date = 2021-07-29T19:30:00.000Z
translationKey = "milleniumbug"
description = "d"
level = "beginner"
speakers = [ "someone" ]
format = "conference"
language = "Italiano"
duration = "60"
draft = false
+++
```

> N.b.: hugo può gestire anche questa struttura in formato yaml ma per coerenza con le altre componenti del sito si sta usando toml (in particolare per farlo funzionare con netlify-cms).

#### Struttura Programma / Schedule

La struttura del programma si compone di 3 file YAML principali, tutti dentro alla cartella `data`: **`rooms.yml`**, **`slots.yml`** e **`schedule.yml`**. I file vanno modificati di concerto perché sono collegati tra loro. In particolare `schedule.yml` è strutturato attraverso riferimenti agli altri due file, Slots e Stanze.

##### `rooms.yml`

In questo file, situato nel folder `data`, si definiscono le stanze disponibili durante l'evento.  
È un array chiamato `rooms` composto da oggetti con i seguenti campi/chiavi

* `key`: usato internamente, non deve avere spazi o caratteri speciali (è quello a cui fa riferimento la pagina di configurazione "Programma")
* `label`: il nome che verrà usato come etichetta nel programma e nei diversi talk
* `description`: la descrizione della stanza che verrà visualizzata nel programma sotto al nome della stessa

##### `slots.yml`

In questo file, situato nel folder `data`, si definiscono gli slot disponibili per i diversi talk. Questa è sicuramente la parte più convoluta del sistema e quindi quella che necessità dell'attenzione maggiore.

È un array chiamato `slots` composto da oggetti con i seguenti campi/chiavi

* `key`: usato internamente, non deve avere spazi o caratteri speciali (è quello a cui fa riferimento la pagina di configurazione "Programma")
* `start`: indica l'orario di inizio dello slot, espresso in minuti:ore ad esempio "11:30"
* `duration`: indica la durata dello slot, espresso in minuti, ad esempio "120"
* `row`: ulteriore oggetto composto dai campi `start` ed `end`. Questo oggetto riguarda unicamente la rappresentazione dello slot nella pagina del programma. Il programma per ogni giorno è una tabella divisa in un numero di colonne pari alle stanze e un numero di righe variabile. I valori numerici che si inseriscono in "start" ed "end" servono a definire dove si collocherà lo slot se verrà selezionato nel programma. Un numero basso lo collocherà nella parte alta della tabella (le righe della tabella vanno contate dall'alto).

##### `schedule.yml`

In questo file, situato nel folder `data`, si struttura l'intero programma, facendo riferimento alle stanze definite in `data/rooms.yml`, agli slot definiti in `data/slots.yml` e ai talk presenti in `content/talks/`. Questo è il cuore del programma.

Il file è composto da un array chiamato `schedule` al cui interno vi sono i giorni dell'evento a loro volta suddivisi in stanze a loro volta suddivise in slot temporali.

**day**

È un array composto dai seguenti campi:

* `day`: la data di un giorno del programma espressa con il seguente formato `YYYY-MM-DD`.
* `start`: l'orario di inizio dei talk del giorno, espresso in `hh:mm`.
* `rooms`: un array con all'interno le stanze che verranno utilizzate per ospitare i talk del giorno

**rooms**

Questo array `rooms` è composto dai seguenti campi:

* `room`: il nome della stanza **!attenzione! il valore inserito in questo campo deve corrispondere al valore `key` di una delle stanze definite nel file `rooms.yml`**
* `slots`: un array che elenca gli slot temporali e il talk asssociato per questa specifica stanza

**slots**

Questo array `slots` è composto dai seguenti campi:

* `slot`: il nome dello slot **!attenzione! il valore inserito in questo campo deve corrispondere al valore `key` di uno degli slot temporali definiti nel file `slots.yml`**
* `talk`: il nome del talk **!attenzione! il valore inserito in questo campo deve corrispondere al valore `key` di uno dei talk presenti in `content/talks`. Questo valore è espresso nella parte iniziale di ciascun file markdown corrispondente a un talk**

**Esempio di un file `schedule.yml`:**

```yaml
schedule:
  - day: '2021-09-03'
    start: '08:30'
    rooms:
      - room: room1 # si riferisce alla stanza come definita in `rooms.yml`
        slots:
          - slot: 0900-60min # si riferisce allo slot come definito in `slots.yml`
            talk: come-dove-perche-installare-linux # si riferisce al talk come definito in `content/talks/<nome-talk>.md
          - slot: 1000-60min
            talk: commodore64-extravaganza
          - slot: lunch
            talk: __lunch
      - room: room2
        slots:
          - slot: 1000-60min
            talk: gancio
          - slot: lunch
            talk: __lunch
```


### Netlify

## Talk

#### Struttura Programma

La struttura del programma si compone di 3 file principali: **Stanze**, **Slots** e **Programma**. I file vanno modificati di concerto perché sono collegati tra loro. In particolare il Programma è strutturato attraverso riferimenti agli altri due file, Slots e Stanze.

##### Stanze

In questa pagina si definiscono le stanze disponibili durante l'evento. Al momento sono 3 ma possono essere di meno o di più.  
All'apertura della pagina ci si troverà di fronte alla lista delle stanze, con possibilità di aggiungerne tramite il pulsante "add stanze".  
Cliccando su una delle stanze già disponibili, si potranno modificare le informazioni relative che sono:

* Nome della stanza: usato internamente, non deve avere spazi o caratteri speciali (è quello a cui fa riferimento la pagina di configurazione "Programma")
* Nome visualizzato della stanza: il nome che verrà usato come etichetta nel programma e nei diversi talk
* Descrizione: la descrizione della stanza che verrà visualizzata nel programma sotto al nome della stessa

##### Slots

In questa pagina si definiscono gli slot disponibili per i diversi talk. Questa è sicuramente la parte più convoluta del sistema e quindi quella che necessità dell'attenzione maggiore.

All'apertura della pagina ci troviamo di fronte a una lista degli slot (un po' come con le stanze). Anche qui, possiamo aggiungere gli slot di tempo che verranno utilizzati nel Programma.  
Ciascuno slot è composto da:

* Nome dello slot: usato internamente, non deve avere spazi o caratteri speciali (è quello a cui fa riferimento la pagina di configurazione "Programma")
* Inizio: indica l'orario di inizio dello slot, espresso in minuti:ore ad esempio "11:30"
* Durata: indica la durata dello slot, espresso in minuti, ad esempio "120"
* Riga: questo campo riguarda unicamente la rappresentazione dello slot nella pagina del programma. Il programma per ogni giorno è una tabella divisa in un numero di colonne pari alle stanze e un numero di righe variabile. I valori numerici che si inseriscono in "Inizio" e "Fine" servono a definire dove si collocherà lo slot se verrà selezionato nel programma. Un numero basso lo collocherà nella parte alta della tabella (le righe della tabella vanno contate dall'alto).

##### `Programma / Schedule`

In questa pagina si struttura l'intero programma, facendo riferimento alle stanze definite in **Stanze**, agli slot definiti in **Slots** e ai talk presenti nella sezione **Talk**. Questo è il cuore del programma.

La pagina è composta da una lista sono elencati i giorni dell'evento, a loro volta suddivisi in una lista di stanze, a loro volta suddivise in una lista di slot temporali e talk ad essi associati.

##### Esempio: cambiare un talk da una stanza a un'altra cambiando anche slot temporale

![cambiare stanza e slot orario a un talk](change-talk-room-slot.gif)