# Sito Hackmeeting 2021 | 0x18

il sito di hackmeeting è statico, come da migliore tradizione, che viene creato, o compilato, da un generatore. Questo generatore funziona per certi versi in maniera analoga a quella di un sito dinamico, con funzioni e varibili che però vengono risolte al tempo della compilazione.

Il sistema è poi composto da diversi pezzi che comprendono la creazione di contenuti tramite interfaccia e la pubblicazione automatica del sito, in produzione, una volta compilato.

[[_TOC_]]

## La struttura

Il sistema di creazione di contenuti del sito è stato concepito per avere la flessibilità di un CMS dinamico e la convenienza (in termini di ridotti tempi di manutenzione) e leggerezza di un sito statico, mantenendo un'unica _source of truth_ per i contenuti. L'obiettivo è permettere a persone con un diverso grado di sapere tecnico di poter contribuire e gestire i contenuti del sito. Per questo il sistema implementato offre anche un'interfaccia grafica per la modifica dei contenuti, oltre alla possibilità di intervenire direttamente sul repository del sito e dei suoi contenuti (in formato markdown).

### Gestione del programma / tabellone dei talk

Il programma dei talk è forse l'elemento più importante del sito. Deve essere modificabile facilmente e velocemente da parte di persone tecniche e non-tecniche. Anche se sotto-sotto c'è una manciata di file markdown e yaml (vedi il file HACKING.md), il metodo "vero" per modificare il programma è modificare un certo calendario su nextcloud a cui puoi chiedere accesso conoscendo persone che stanno seguendo il sito. Apri una issue se vuoi avere accesso e poter modificare il programma!

### Tasselli del mosaico

Questi sono i diversi elementi che compongono questo sistema:

* __[Hugo](https://gohugo.io/)__, il generatore di siti statici in Go. Viene utilizzato (surprise!) per generare il sito.
* __[netlify-cms](https://www.netlifycms.org/)__, una interfaccia per la gestione di contenuti. Viene utilizzato come backend per collegarsi a questo repository e modificare i file markdown (`.md`) dei contenuti (folder `content`) o i file di configurazione del programma `data/schedule.yml`). Utilizza Gitlab per l'autenticazione.
* __[Webdav-upload](https://git.autistici.org/ai3/tools/webdav-upload)__, software in Python3 per caricare file tramite webDAV. Questo software viene utilizzato nella pipeline di pubblicazione per caricare il sito in produzione tramite webDAV. ***Publish as if it were 2003! With the tools of 2021.***

### Hugo

È il software che è stato utilizzato per il sito di hackmeeting nel 2014. È simile a Pelican, usato gli scorsi anni.
[...]

### Netlify-cms

Si tratta di una piccola applicazione in React (javascript) che permette di collegarsi al repository del sito - utilizzando il meccanismo di autenticazione di gitlab - e di modificare i file markdown (ma anche yaml e toml) attraverso un'interfaccia grafica.

L'interfaccia grafica viene creata a partire da un file di configurazione presente in [`static/admin/config.yml`](static/admin/config.yml). In questo file vengono descritti i diversi tipi di contenuto e i campi che li compongono. [...]

### Pipeline di pubblicazione

[...]

## Come funziona? Come posso contribuire?

La prima cosa che ti serve per contribuire è un utente sulla piattaforma [0xacab.org](https://0xacab.org), istanza gitlab legata a riseup.

### Utente specifico

 Puoi registrarti a 0xacab da [qui](https://0xacab.org/users/sign_up). A questo punto puoi:
 
 * __Richiedere i permessi per scrivere sul repository con il tuo utente specifico__

 Nella pagina del progetto, fare click su `Request access`, come spiegato [qui](https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project).

 * __Fare un fork del repository e richidere successivamente un merge del codice modificato__

 Qualora si volesse fare una modifica una tantum, ad esempio si vuole proporre solo un talk, è possibile copiare il repository con il proprio utente, effettuare le modifiche del caso e poi fare richiesta di merge, _merge request_ al repository "ufficiale". Gli step sono descritti [qui](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork).

 ### Utente condiviso

C'è anche un utente _condiviso_: [hackmeeting](https://0xacab.org/hackmeeting). Chiedine la password scrivendo a chi credi la possa sapere :)

## Come pubblicare un contenuto sul sito

Ci sono due modalità principali per intervenire sui contenuti del sito, pubblicandone di nuovi o modificando quelli esistenti.

### Via modifica diretta del codice tramite git

Questa soluzione è valida per tutti i "tipi" di utenti che sono stati descritti sopra, dall'account condiviso a quello personale che non ha accesso diretto al repository e lavora sul proprio fork, la propria copia, del sito.

**Installo Hugo nel mio sistema** seguendo la documentazione [qui](https://gohugo.io/getting-started/installing/).

**Clono il repository**

```
git clone <URL REPO>
```

**Lancio Hugo in modalità sviluppo**

Questo comando non fa altro che compilare il sito e "servirlo" con un piccolo server in Go (porta di default 1313):

```
hugo serve -b localhost/hackit21/
```

**Compilo il sito con Hugo**

```
hugo
```


### Via netlify-cms, l'interfaccia grafica

Per questa strada è necessario avere un account abiliato, cioè con i giusti permessi, per scrivere nel repository del sito. L'account condiviso e l'account personale che abbia fatto richiesta di accedere al repository rientrano in questa categoria.

(L'[editorial workflow di netlify-cms](https://www.netlifycms.org/docs/configuration-options/#publish-mode), [supportato in beta](https://www.netlifycms.org/docs/beta-features/#gitlab-and-bitbucket-editorial-workflow-support), non permette di fare il fork da interfaccia grafica, ma crea nuovi branch.)

#### Login

Per collegarsi all'interfaccia grafica di gestione del sito:
* visitare l'indirizzo [hackmeeting.org/hackit21/admin](https://hackmeeting.org/hackit21/admin)
* cliccare sull'unico pulsante _Login with Gitlab_. Si verrà reindirizzati sull'istanza gitlab 0xacab.org
* effettuare il login su 0xacab con le credenziali a disposizioni - le proprie o quelle dell'utente `hackmeeting`
    * La prima volta che si effettua il login a netlify-cms con il proprio utente (quindi non l'utente `hackmeeting`), 0xacab vi chiederà di autorizzare l'applicazione

Si verrà infine reindirizzate alla pagina principale dell'interfaccia di gestione: sulla sinistra  è presente un menù con i diversi tipi di contenuto, sulla destra i singoli contenuti appartenenti a quel tipo. Ad esempio, al login viene aperto il tipo di contenuto "pagine" che al suo interno elenca la homepage in italiano e la call per i contributi.

#### Pagine

Sono le pagine "fisse" del sito, come la homepage e la call for contributions. Hanno titolo e data come campi disponibili, oltre al corpo del testo. In quest'ultimo oltre al testo è possibile anche inserire degli elementi grafici tramite _shortcode_, dei tag che hanno questa struttura `{{< shortcode variabile=valore >}} {{< /shortcode >}}`. Per un elenco degli shortcode disponibili vedi [qui](https://github.com/GDGToulouse/devfest-theme-hugo#home).

##

#### Warmup
