+++
title = "HackIT 0x18"
date = 2021-06-23T18:42:49.000Z
+++
{{< jumbo img="./uploads/mondinedupont.jpg" >}}

<h1>Hackmeeting 0x18</h1>

<h2>3-5 Settembre 2021</h2>
<h3><a href="https://casadelpopoloponticelli.noblogs.org/contatti/">Casona di Ponticelli (Bologna)</a></h3>
<h3><a href="#dove">Dove?</a></h3>
{{< button-link label="Call for contributions" icon="subscribe" url="cfp" >}}
{{< /jumbo >}}

{{% home-info %}}

## Cos'è Hackmeeting?

L’hackmeeting è l’incontro annuale delle controculture digitali italiane, di quelle comunità che si pongono in maniera critica rispetto ai meccanismi di sviluppo delle tecnologie all’interno della nostra società. Ma non solo, molto di più. Lo sussuriamo nel tuo orecchio e soltanto nel tuo, non devi dirlo a nessuno: l’hackit è solo per veri hackers, ovvero per chi vuole gestirsi la vita come preferisce e sa s/battersi per farlo. Anche se non ha mai visto un computer in vita sua.

Tre giorni di seminari, giochi, dibattiti, scambi di idee e apprendimento collettivo, per analizzare assieme le tecnologie che utilizziamo quotidianamente, come cambiano e che stravolgimenti inducono sulle nostre vite reali e virtuali, quale ruolo possiamo rivestire nell’indirizzare questo cambiamento per liberarlo dal controllo di chi vuole monopolizzarne lo sviluppo, sgretolando i tessuti sociali e relegandoci in spazi virtuali sempre più stretti.

**L’evento è totalmente autogestito: non ci sono organizzatori e fruitori, ma solo partecipanti.**

![lmanifesto hm2021](https://hackmeeting.org/hackit21/uploads/loc_hackit2021.png "hackmeeting 2021")

{{% /home-info %}}

{{% home-location
image="/hackit21/images/map.png"
address="Via Ponticelli 43, 40051 Malalbergo"
latitude="44.6983"
longitude="11.4744" %}}

## Dove

### "La Casona" di Ponticelli

La Nuova casa del popolo di Ponticelli di Malalbergo, situata a nord di Bologna.
{{% /home-location %}}

{{% album images="./uploads/casona/spazioinfanzia.jpg,./uploads/casona/spaziograndefrontecucina.jpg,./uploads/casona/hackit0x18.png,./uploads/casona/spazioesposizioni.jpg,./uploads/casona/mappacasona.jpg,./uploads/casona/entrando.jpg,./uploads/casona/alberitendedalontano.jpg,./uploads/casona/parcheggio.jpg" %}}

### Ecco La Casona che ci ospita... campeggio, tendoni, prati,... No al polo logistico!
https://casadelpopoloponticelli.noblogs.org/polo-logistico-altedo/ 

{{% /album  %}}

{{< jumbo img="/images/somma_inversa.jpg" >}}
<h1>Hackmeeting 0x18</h1>
<h2>3-5 Settembre 2021</h2>
{{< /jumbo >}}

{{< home-warmups >}}
<h2 id="warmup"> Eventi di avvicinamento, <i>warmups</i>:</h2>
{{< /home-warmups >}}
