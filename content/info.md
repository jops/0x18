+++
title = "Info"
date = 2021-06-23T18:42:49.000Z
layout = "cfp"
menu = "main"
brief = "Cose da sapere. Per orientarsi e organizzarsi."
+++
## Info pratiche

**Quando** 3 - 4 - 5 settembre 2021

**Dove** Nuova Casa del Popolo di Ponticelli "La Casona" a Malalbergo (BO).  ([su OpenStreetMap](https://www.openstreetmap.org/way/152728721))

**Mappa del luogo** con la [descrizione dei diversi spazi interni](https://lab.dyne.org/Hackmeeting2021?action=AttachFile&do=get&target=casona.png)

**State facendo un incontro nonostante il COVID-19? non è pericoloso?** Stiamo prendendo [tutte le precauzioni necessarie](/hackit21/infocovid/)

### Dormire

Nello spazio "parcheggio" (circa 5000mq) si possono parcheggiare macchine e camper; non c'e' ombra. Per i camper si può caricare acqua vicino al bagno, possibilità di scarico a pochi KM ( https://www.openstreetmap.org/way/369872180 ).

Nello spazio "alberi" (circa 1500mq) si possono mettere esclusivamente tende. ci sono un po' di alberi e di ombra, il suolo e' prato, soffice e ben picchettabile.

Non ci sono spazi al chiuso dove dormire.

All'imbrunire ci sono zanzare. Grosse.

### Mangiare

Ci saranno colazioni/pranzi/cene (vegetali e non) ad un prezzo popolare, non ci sono bar o negozi raggiungibili a piedi dalla casona. Ci sono due trattorie a circa 2km di distanza.

#### Distribuzione del cibo

Al banchetto infopoint all’ingresso si comprano i biglietti per i pasti. Per piacere, comprali con anticipo: questo aiuta chi cucina a fare i conti del numero di pasti necessari.

#### Bicchieri

I bicchieri usa e getta sono molto inquinanti, quindi se puoi portati un bicchiere da casa. In questo modo potrai riutilizzarlo senza sprecarne uno ogni volta.

### Bagni

Ci saranno 3 WC con lavandino.

Ci saranno 3 docce a disposizione accanto al bagno, con rudimentale acqua calda solare.

## Info teoriche

**Chi organizza l’hackmeeting?** L’hackmeeting è un momento annuale di incontro di una comunità che si riunisce intorno a una mailing list. Non esistono organizzatori e fruitori. Tutti possono iscriversi e partecipare all’organizzazione dell’evento, semplicemente visitando il sito www.hackmeeting.org ed entrando nella comunità.

**Chi è un hacker?** Gli hacker sono persone curiose, che non accettano di non poter mettere le mani sulle cose. Che si tratti di tecnologia o meno gli hackers reclamano la libertà di sperimentare. Smontare tutto, e per poi rifarlo o semplicemente capire come funziona. Gli Hackers risolvono problemi e costruiscono le cose, credono nella libertà e nella condivisione. Non amano i sistemi chiusi. La forma mentis dell’hacker non è ristretta all’ambito del software-hacking: ci sono persone che mantengono un atteggiamento da hacker in ogni campo dell’esistente, spinti dallo stesso istinto creativo.

**Chi tiene i seminari?** Chi ne ha voglia. Se qualcuno vuole proporre un seminario, non deve fare altro che proporlo in lista. Se la proposta piace, si calendarizza. Se non piace, si danno utili consigli per farla piacere.

**Ma cosa si fa, a parte seguire i seminari?** Esiste un “lan-space”, vale a dire un’area dedicata alla rete: ognuno arriva col proprio portatile e si può mettere in rete con gli altri. In genere in questa zona è facile conoscere altri partecipanti, magari per farsi aiutare a installare Linux, per risolvere un dubbio, o anche solo per scambiare quattro chiacchiere. L’hackmeeting è un open-air festival, un meeting, un hacking party, un momento di riflessione, un’occasione di apprendimento collettivo, un atto di ribellione, uno scambio di idee, esperienze, sogni, utopie.

**Quanto costa l’ingresso?** Come ogni anno, l’ingresso all’Hackmeeting è del tutto libero; ricordati però che organizzare l’Hackmeeting ha un costo. Le spese sono sostenute grazie ai contributi volontari, alla vendita di magliette e altri gadget e in alcuni casi all’introito del bar e della cucina.

**Cosa posso portare** Se hai intenzione di utilizzare un computer, portalo accompagnato da una ciabatta elettrica. Non dimenticare una periferica di rete di qualche tipo (vedi cavi ethernet, switch e/o dispositivi WiFi). Ricordati inoltre di portare tutto l’hardware su cui vorrai smanettare con gli altri. Durante l’evento la connessione internet sarà estremamente limitata dato il posto ma, se vuoi essere assolutamente sicuro di poterti connettere, portati una pennina 4G e il necessario per condividerla con gli amici! In generale, cerca di essere autosufficiente sul lato tecnologico.

**Posso arrivare prima di venerdì?** Vuoi arrivare qualche giorno prima? Fantastico! Nei giorni precedenti ad hackmeeting ci sono sempre molte cose da fare (preparare l’infrastruttura di rete, preparare le sale seminari, costruire le docce, la segnaletica, e tanto altro!) quindi una mano è ben accetta. Dai un’occhiata alla mailing list per organizzarti.

**Posso scattare foto, girare video, postare, taggare, condividere, uploadare?** Pensiamo che ad ogni partecipante debba essere garantita la libertà di scegliere in autonomia l’ampiezza della propria sfera privata e dei propri profili pubblici; per questo all’interno di hackmeeting sono ammessi fotografie o video solo se chiaramente segnalati e precedentemente autorizzati da tutte e tutti quanti vi compaiano. Le persone che attraversano hackmeeting hanno particolarmente a cuore il concetto di privacy: prima di fare una foto, esplicitalo.

**Come ci si aspetta che si comportino tutte e tutti?** Hackmeeting è uno spazio autogestito, una zona temporaneamente autonoma e chi ci transita è responsabile che le giornate di hackit si svolgano nel rispetto dell’antisessismo, antirazzismo e antifascimo. Se subisci o assisti a episodi di oppressione, aggressione, brute force, port scan, ping flood e altri DOS non consensuali e non sai come reagire o mitigare l’attacco, conta sul sostegno di tutta la comunità e non esitare a richiamare pubblicamente l’attenzione e chiedere aiuto.

**C'è internet? Come si usa il wifi?** Trovi tutte le info tecniche [qui](/hackit21/tech/)

### Info brevi in castigliano

Te quieres pasar al hackmeeting pero no hablas italiano?
no estras sol@s! aunque no habrá traducciónes en tiempo real de todos
los nodos estamos organizadas por hacer grupos de traducción depediendo
de las nececidades. sea por quien quiere ir a una charla o si queres darla.

### Info brevi in inglese

You want to come to the hackmeeting but you don't speak italian?
You're not alone! Even if we won't translate all the talks in real time
we are organized to make translation groups according to the needs,
whether it's to follow a talk or to do it.