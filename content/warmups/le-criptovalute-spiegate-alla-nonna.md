+++
title = "Le criptovalute spiegate alla nonna"
image = "https://hackmeeting.org/hackit21/uploads/flyer-hm.png"
date = 2021-07-28T08:13:04.504Z
brief = "È possibile pagare il droghiere virtuale rimanendo anonimə?"
when = 2021-07-30T21:00:40.541Z
venue = "FOA Boccaccio 003"
address = "Via Timavo 12, Monza"
+++
Il collettivo [lab61](https://www.lab61.org) e l’assemblea di gestione di [Nebbia](https://nebbia.fail), due comunità nate intorno alla critica dei meccanismi dello sviluppo tecnologico, invitano tuttə a fare quattro chiacchiere sul tema delle **criptovalute**.

Scegliamo di essere parte del percorso che porta ad [Hackmeeting 0x18](https://hackmeeting.org/hackit21/), l’incontro annuale delle controculture digitali italiane, che quest’anno si terrà dal **3 al 5 Settembre** a [Casona di Ponticelli (Bologna)](https://casadelpopoloponticelli.noblogs.org/contatti/).

Vi invitiamo a costruire insieme un momento di “riscaldamento” per **Hackmeeting**, per (ri)abituarci a parlare di tecnologia tra antagonistə, per promuovere le pratiche che adottiamo nelle strade e nelle piazze reali anche negli spazi virtuali, per costruire azione dal basso.

Parleremo di criptovalute non certo per amore nei confronti della finanza, ma perchè pensiamo che ogni nuova tecnologia vada innanzitutto studiata.\
Noi l’abbiamo fatto e abbiamo scoperto delle cose interessanti. Anche per la nonna.

Saremo ospiti della [FOA Boccaccio di Monza](https://boccaccio.noblogs.org/) e di questo siamo orgogliosə.

![](https://hackmeeting.org/hackit21/uploads/flyer-hm.png)