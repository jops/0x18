+++
title = "Cyberbluff"
date = 2021-08-05T11:22:17.395Z
brief = "Presentazione del libro Cyberbluff con l'autore"
when = 2021-08-29T20:30:17.414Z
venue = "Nuova Casa del Popolo di Ponticelli \"La Casona\" a Malalbergo (BO)"
address = "Via Ponticelli, 43, 40051 Ponticelli BO"
+++
Utilizziamo internet per qualsiasi cosa: informarci, acquistare, studiare, divertirci, comunicare. Spesso ci sembra impossibile distinguere la rete dal servizio che si utilizza: il web è Google, Instagram, Facebook. Sono poche le persone che si interrogano sulla rete a cui questi servizi si appoggiano per capire da dove viene, com’è nata e da chi è controllata. Sappiamo come funziona questo strumento, questa rete che è onnipresente nelle nostre vite? Sappiamo dove vanno i nostri dati e chi ne trae profitto? Abbiamo davvero bisogno di comunicazioni istantanee? Internet è davvero democratico? Questo libro descrive i servizi online più diffusi, raccontandone la genesi, il funzionamento, e fornendo alcuni consigli utili per utilizzarli in maniera più consapevole.