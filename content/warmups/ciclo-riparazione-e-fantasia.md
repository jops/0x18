+++
title = "Ciclo riparazione e fantasia"
image = "https://ampioraggio.noblogs.org/files/2021/08/ciclo_hm.png"
date = 2021-09-01T09:49:21.691Z
brief = "Riparazione bici (principalmente freni e ruota) e fantasia! Porta la tu bici!  Mercoledì 1 settembre alle 19 "
when = 2021-09-01T19:00:21.787Z
venue = "Circolo Anarchico Camillo Berneri"
address = "p.zza di Porta S. Stefano 1"
+++
Hackmeeting, l’incontro annuale delle controculture digitali italiane, quest’anno sarà alla Casona del Popolo di Ponticelli di Malalbergo (San Pietro in Casale, Bo). Andiamoci in sella!!
– WARMUP CICLICO –

Riparazione bici (principalmente freni e ruota) e fantasia! Porta la tu bici!

Mercoledì 1 settembre alle 19 @ Circolo Anarchico Camillo Berneri (p.zza di Porta S. Stefano 1)
– TUTTI IN BICI VERSO HACKMEETING –

Biciclettata da via Gobetti (rotonda Xm24) alla Casona del Popolo di Ponticelli (Bo)

Venerdì 3 settembre alle 18.30

Ricordati di portare lucine e camere d’aria! La pedalata durerà circa 2 ore (27 km) E arriveremo qui: https://www.openstreetmap.org/#map=19/44.69833/11.47471

Porta la tenda o l’amaca se vuoi fermarti a dormire (scelta più che consigliata) oppure treni per il ritorno dalla stazione di S. Pietro in Casale fino alle 22.30 (però sabato tornate ad hackmeeting, altra scelta consigliata).

Hackmeeting è un evento autogestito, per maggiori informazioni ( su programma, ospitalità e altro) visita il sito hackmeeting.org

SALVIAMO LE RISAIE!

La Casona di Ponticelli si trova vicino alle risaie di Altedo, le ultime del bolognese, ora minacciate dalla costruzione di un hub logistico. Info sul sito della Casona di Ponticelli