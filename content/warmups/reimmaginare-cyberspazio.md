+++
title = "Possiamo (re)immaginare il cyberspazio?"
brief = "Chiacchierata su Fediverso e autogestione digitale + presentazione di Discourse"
date= 2021-07-13T13:42:49+01:00
venue = "Vag61"
address = "Via Paolo Fabbri, 110"
when = 2021-07-22T19:00:49+01:00
+++

Le nostre interazioni digitali sono sempre piú definite e controllate da una manciata di grosse aziende multinazionali. Opporsi a questo stato di cose e (re)immaginare una rete diversa è possibile. Le alternative libere e autogestite ai social network e alle piattaforme digitali commerciali non solo esistono, ma continuano a proliferare. Da qualche anno molte di queste alternative si raccolgono attorno a un luogo del cyberspazio che si chiama Fediverso: un universo di server federati. Ne parliamo insieme al collettivo Bida, che presenterà anche un nuovo progetto: Discourse, una piattaforma in stile forum per discussioni e confronti.