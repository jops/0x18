+++
title= "Schedule"
menu = "main"
horizontal = false
outputs = ["HTML", "RSS", "calendar"]
+++

{{< hero >}}

<a class="btn primary btn-lg" href="webcals://it.hackmeeting.org/schedule.ics">
    <svg class="icon icon-calendar"><use xlink:href="#calendar"></use></svg> ICal
</a>

<a class="btn primary btn-lg" href="https://ggt.gaa.st/#url=https://it.hackmeeting.org/schedule.ics" style="margin-left: 2em;">
    <svg class="icon icon-calendar"><use xlink:href="#calendar"></use></svg> Aggiungi ad Android
</a>

{{< /hero >}}

Gli audio delle registrazioni sono disponibili [su arkiwi](https://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMjE/html)

Qui invece trovi [slide e altri materiali](https://hackmeeting.org/media/hackit21/)
