---
duration: 45
end: 2021-09-04 20:15:00+02:00
format: conference
key: ac0a8ddd-7436-46fa-983f-b9cf47e1fefe
location: room2
start: 2021-09-04 19:30:00+02:00
tags: []
title: SPID Aruba e Lepida senza app
---

Usare lo spid di Lepida, o di Aruba, con qualunque applicazione TOTP.

Le conoscenze tecniche richieste non sono molte (lanciare qualche programmino), parleremo anche di come queste aziende nascondono protocolli standard dietro app di loro controllo esclusivo.