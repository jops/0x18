---
duration: 60
end: 2021-09-04 12:00:00+02:00
format: conference
key: 1218beb9-30cb-4237-bb55-c4c37b4d504a
location: room1
start: 2021-09-04 11:00:00+02:00
tags: []
title: Smash the filter bubble! PORTATE IL PC!!!! E' UN WORKSHOP!!!!
---

Gli algoritmi dei social media sono software proprietari inaccessibili come
una "black box", ma sono in grado di influenzare chi li subisce. Rappresentano
l'opposto all'autodeterminazione nel mondo digitale. Moltə di noi pensano
"bah, io posso uscire dalla mia bolla" e non lo mettiamo in discussione, ma
come possiamo fare uscire le bolle dai nostri computer per mostrare a tuttə
 gli effetti della profilazione? Come possiamo dimostrare la discriminazione
compiuta da questi algoritmi?

Tracking.exposed è free software che con un estensione browser colleziona
passivamente le raccomandazioni algoritmiche di youtube, pornhub, facebook e
amazon. Ad hackmeeting parleremo delle prime 2 piattafome, e faremo
un'osservazione collettiva di uno dei loro algoritmi, collezionando evidenze
che potremo comparare per capire quanto la rappresentazione di un tema possa
cambiare grazie a un algoritmo che suggerisce contenuti diversi a noi rispetto
a chi siede nella sedia vicino alla nostra, a dei bot amanti degli unicorni
rispetto al nostro genitore più conspirazionista.