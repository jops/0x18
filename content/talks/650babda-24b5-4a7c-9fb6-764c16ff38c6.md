---
duration: 60
end: 2021-09-04 16:00:00+02:00
format: conference
key: 650babda-24b5-4a7c-9fb6-764c16ff38c6
location: room1
start: 2021-09-04 15:00:00+02:00
tags: []
title: L'internet delle (mie) cose
---

Uno sguardo critico (ma anche no) sulle tecnologie per il benessere delle donne.
Per FemTech si indicano gli oggetti per la salute delle donne che acquisiscono intelligenza accedendo a informazioni del nostro corpo, come le app che tengono traccia dell’ovulazione e del flusso mestruale, o i > sextoys che misurano gli orgasmi. L’internet delle (mie) cose mappa il nostro corpo fisico restituendoci un’identità elettronica, ma i nostri dati biometrici diventano proprietà di società private. Il tema sulle femtech, come quello degli assistenti, anzi, delle assistenti virtuali, non riguarda solo i dati e la privacy, ma anche gli algoritmi, i codici, il design. Dalla rappresentazione visiva, agli oggetti dedicati alla cura di se e degli altri, il design è un processo di rappresentazione che sintetizza le disuguaglianze di genere, razza e classe della società.