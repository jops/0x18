---
duration: 60
end: 2021-09-04 17:45:00+02:00
format: conference
key: 5fc298e4-d078-42c4-95ff-42de7a0273c0
location: room3
start: 2021-09-04 16:45:00+02:00
tags: []
title: Mirabolanti avventure del vostro vetusto apparecchio GSM
---

Mentre il dibattito interplanetario si scalda per l'annosa questione del 5G, avrete sentito l'impellente necessità di trovare un argomento differente su cui sproloquiare nel gruppo WhatsApp della Pro Loco.

Ma allora perché non riprendere un protocollo di comunicazione che doveva
essere morto e sepolto tra il secondo e il terzo governo Berlusconi e che invece, come il nostro padre spirituale, è ancora vivo e lotta per restare nei nostri dispositivi.

Insomma, vista la sua longevità sarebbe carino metterci con un po di allegria a cercare di tirare su una nostra rete GSM.

Proveremo ad usare osmocom, mirabolante stack softwareche riesce ad implementare l'impensabile, ed una timida LimeSDR, che insomma non sarà il pezzo di hardware più adeguato allo scopo ma a livello sperimentale è più che soddisfacente. Con la scusa di implementare una cella funzionante vedremo anche una parte del funzionamento di GSM e le sue principali lacune di sicurezza.
Se avremo tempo e voglia a fine talk presenterò un ruolo ansible che
dovrebbe automatizzare la configurazione di una cella funzionante.
