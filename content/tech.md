+++
title = "Info su cose tecniche"
date = 2021-09-02T18:42:49.000Z
layout = "cfp"
brief = "Info tecniche"
+++

## Wi-Fi

Ad hackmeeting è disponibile rete wi-fi. Una rete (`hackit-insecure`) è senza password; di conseguenza tutto il tuo traffico ethernet fino all'access point girerà in chiaro. Un'altra rete (`hackit`) richiede autenticazione tramite credenziali. Le credenziali sono in realtà pubbliche: `username=hackit`, `password=hackit`, ma nonostante questo il traffico viene crittografato con chiave diversa per ogni sessione, garantendo una sicurezza maggiore.

### Su linux

-  scaricate [il certificato](https://hackmeeting.org/hackit21/hackit_wifi.crt).
- collegatevi a `hackit`
- Inserite
   - Identità anonima: `hackit`
   - Username: `hackit`
   - Password: `hackit`
   - Autenticazione: `PEAP`
   - Inner authentication: `MSCHAPv2`
   - Certificato: il file scaricato in precedenza
- collegatevi

![](/hackit21/uploads/howto-wifi-linux.png)
