+++
title = "Info covid"
date = 2021-08-18T18:42:49.000Z
layout = "cfp"
brief = "Info covid"
+++
L'Hackmeeting è un evento autogestito: la tutela della salute di tutte e tutti dipende anche da te. Fai attenzione alle misure di prevenzione del covid, che tu sia vaccinata o no. In particolare, l'uso della mascherina è necessario per tutti e tutte in ogni ambiente chiuso (cucina, bar e bagni), nei luoghi di distribuzione di cibo e bevande, e in ogni situazione in cui per qualche motivo non è possibile mantenere una distanza adeguata.

In questa pagina cercheremo di dettagliare tutte le misure che stiamo prendendo.

## Spazi aperti

Tutto avverrà all'aperto. I seminari, il lan space, le docce, ovviamente l'area tende... tutto.

La cucina avrà una parte al chiuso (una parte dei fornelli, nemmeno tutti!) e una parte all'aperto, in modo
che chi deve fare cose come tagliare le verdure o altri tipi di preparazioni che non richiedono di stare
vicino al fuoco non vada ad affollare la cucina.

### E se piove?

Gli spazi sono aperti ma in gran parte coperti. Le coperture sono molto alte, quindi non creano problemi al
passaggio d'aria. Le tende ovviamente non hanno copertura, così come le docce

## Igienizzante

Alla Casona si troveranno dispenser di igienizzante in molti punti. Tuttavia chiediamo a tutte e tutti
i partecipanti di munirsi di un dispenser personale (tipo di quelli tascabili...), e ovviamente di usarlo
spesso.

Ricordiamo in ogni caso che lavarsi le mani con acqua e sapone è meglio che usare l'igienizzante: costa meno,
pulisce di più, non ti butta litri di alcol sulla pelle. 

## Stoviglie

Piatti bicchieri e posate avranno una procedura di sanificazione studiata appositamente:

1. lavaggio sommario con il solito metodo delle 3 vaschette. ognuno lava il suo
   (ricordandosi di lavarsi le mani prima e dopo)
2. messi a mollo nell'ipoclorito di sodio, per disinfettarli
3. risciacquo rapido per togliere l'ipoclorito di sodio
4. ulteriore risciacquo utilizzando una lavapiatti da ristorante ad alta temperatura

Queste procedura ci sembrano più che adeguate (verosimilmente superiori a quelle di molti locali commerciali)
ed evitano l'usa e getta che produce un gran numero di rifiuti.

## Distribuzione del cibo

Al banchetto dell'infopoint (che in pratica sta all'ingresso insieme alla sottoscrizione) si comprano
i biglietti per i pasti. Questi biglietti sono numerati. All'ora di pranzo, la cucina chiama 
"blocchi" di numeri. Chi ha quel numero andrà alla cucina a ritirare il piatto.

In questo modo:

* si evita completamente che il passaggio di soldi si incontri col passaggio di cibo
* chi fa la distribuzione poggia solo i piatti sul tavolo, e chi va lì lascia il suo bigliettino e si prende
    il piatto, di nuovo evitando il contatto
* raggruppando i blocchi di numeri si evitano le file e quindi gli assembramenti

## Bagni

I WC hanno di fianco un lavello per potersi lavare le mani. Il materiale per la pulizia dei bagni
sarà sempre disponibile in modo che, oltre ai turni di pulizia, tutti possano pulire il bagno dopo averlo
usato.

## Dormire

Si può dormire solo in tenda, quindi...

## Tampone rapido

Ad hackmeeting abbiamo una scorta di tamponi rapidi (in numero limitato) e di mascherine.
