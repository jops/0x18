+++
title = "Come arrivare"
date = 2021-06-23T18:42:49.000Z
layout = "cfp"
menu = "main"
brief = "Come arrivare!"
+++
# Come arrivare

Il posto è qui:   https://www.openstreetmap.org/#map=19/44.69833/11.47471

Coordinate maidenhead: JN54RQ67WO

## Come arrivare a Ponticelli senza macchina.

### Arrivare coi treni da Bologna:

costo: 3,05€, durata del viaggio variabile fra i 16 e i 29 minuti 

Bologna - San Pietro in Casale: dalle 6 alle 19 due treni ogni ora (a e 12 e a e 25) con qualche rinforzo (9:45, 13:45, 14:36, 16:45, 17:45). Ultimi treni: 20:25, 21:12, 22:35, 00:02 

San Pietro in Casale - Bologna: tendenzialmente due all'ora fino alle 19 con qualche rinforzo. Ultimi treni: 20:33, 21:24, 22:30 

### Arrivare coi treni da Ferrara:

costo: 3,05€, durata del viaggio variabile fra i 12 e i 20 minuti 

Ferrara - San Pietro in Casale: tendenzialmente uno all'ora (a e 5) con qualche rinforzo in orario pranzo. Ultimo treno: 22:12

#### Una volta arrivati a San Pietro in Casale

* **A piedi (1h e 20')** o **in bici (15')** dalla stazione: https://www.openstreetmap.org/directions?engine=fossgis_osrm_foot&route=44.6983%2C11.4747%3B44.6991%2C11.4029#map=14/44.7021/11.4496
* **Navetta con un furgone con 8 posti messo a disposizione da Hacklabbo**, saranno garantite queste corse in partenza dalla stazione:  
_Giovedì_ alle 18:15 e 20:15, _Venerdì_ alle 10:15, 15:15, 20:15 e 23:15 e _Sabato_ alle 10:15, 12:15, 14:15, 18:15 e 20:15. _Puoi segnarti nei turni per fare da autista della navetta [qui](https://pad.gattini.ninja/p/hackarrozza)_. Ci sarà una bacheca durante hackmeeting per prenotare le corse di ritorno (sconsigliate durante l'assemblea, ovvero tarda mattinata di domenica)
* Ci sarà un condividi-bici nella tratta di 7Km Ponticeli/San Pietro in Casale stazione (con lucchetti a combinazione)

- - -

### Bus:

* [TPER 356](https://www.tper.it/content/linea-356-bologna-altedo-malalbergo-ferrara)
  (fermata più vicina, Pegola, 36' a piedi)
* [TPER 357](https://www.tper.it/content/linea-357-bologna-altedo-passosegni)
  (fermata più vicina, Altedo) 

- - -

### In bici:

* Da Bologna: 26km, circa: 1h31'. [Strada bella e poco trafficata in generale](https://www.openstreetmap.org/directions?engine=graphhopper_bicycle&route=44.5068%2C11.3448%3B44.6981%2C11.4750#map=11/44.6024/11.4100)
* Da Ferrara: 26km, circa: 1h28'. Strada meno bella

- - -

### Barca:

Vicino ci sono il canale Reno e il canale Navile, non sappiamo quanto navigabili.

- - -

## Come arrivare a Ponticelli con la macchina:

Chiedete a tutti gli amicu se han bisogno un passaggio.

* Per strade normali: da Bologna via S. Marino di Bentivoglio - Saletto fino alla SP20, poi vedi sotto.
* Via autostrada A13, uscire ad Altedo e imboccare la SP20 in direzione S. Pietro in Casale. Dopo 1,5km svoltare a destra per via Ca' Bianca poi un altro km fino a Ponticelli.

E' disponibile una colonnina di ricarica ENEL a poche centinaia di metri.
